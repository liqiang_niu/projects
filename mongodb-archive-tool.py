#! /usr/bin/python
#_*_ coding: utf-8_*_

#niuliqiang 2014-10-23
#mongodb archive tool

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import time
import pymongo
import random

###

mongo_ip = '121.40.193.156'
mongo_port = 27017

log_file = 'log.txt'
count = 0

limits = 30000
max_count = 100000

###

all_users_database_list = list()
user_weibo_time = 'time'
user_bbs_time = 'time'
user_blog_time = 'publish_time'
user_np_time = 'publish_time'
user_nw_time = 'publish_time'

########################################
def readAllUsersFromMongoDB():
    client = pymongo.MongoClient(mongo_ip, mongo_port)
    all_users_db = client['all_users_db']
    all_users_info = all_users_db['all_users_info']
    del all_users_database_list[:]
    for item in all_users_db['all_users_info'].find({'u_name':{'$exists':'true'}}):
        u_mongodb = item['u_mongodb']
        if u_mongodb is not None and u_mongodb != '':
            all_users_database_list.append(u_mongodb)
    ##
    print all_users_database_list
    client.close()
#########################################
def archiveMongodbEveryMonth():
    global count
    ###mongodb connection
    conn = pymongo.Connection(mongo_ip, mongo_port)
    ###current month
    currentTime = time.strftime('%Y-%m',time.localtime(time.time()))
    #print 'time is : ',currentTime
    #every user database
    for item in all_users_database_list:
        u_db_name = item
        print u_db_name
        u_db = conn[u_db_name]
        ### weibo
        u_collection_weibo = u_db['user_weibo']
        print 'user weibo'
        if u_collection_weibo.count() > 0:
            ###find data in last month
            print 'find...'
            for u_weibo in u_db['user_weibo'].find({user_weibo_time:{'$lt':currentTime}}).limit(limits):
                #print u_weibo
                u_weibo_time = u_weibo[user_weibo_time]
                #print u_weibo_time
                u_weibo_year = u_weibo_time[0:4]
                u_weibo_month = u_weibo_time[5:7]
                #print u_weibo_year,u_weibo_month
                archive_weibo_collection = 'user_weibo_'+u_weibo_year+'_'+u_weibo_month
                u_db[archive_weibo_collection].insert(u_weibo)
                u_db['user_weibo'].remove(u_weibo)
                count = count+1
            print 'count: ',count
            print 'remove...'
            #u_db['user_weibo'].remove({user_weibo_time:{'$lt':currentTime}})
        else:
            print 'no weibo!'
        ###
        u_collection_bbs = u_db['user_bbs']
        print 'user bbs'
        if u_collection_bbs.count() > 0:
            ###find data in last month
            for u_bbs in u_db['user_bbs'].find({user_bbs_time:{'$lt':currentTime}}).limit(limits):
                #print u_bbs
                u_bbs_time = u_bbs[user_bbs_time]
                #print u_bbs_time
                u_bbs_year = u_bbs_time[0:4]
                u_bbs_month = u_bbs_time[5:7]
                #print u_bbs_year,u_bbs_month
                archive_bbs_collection = 'user_bbs_'+u_bbs_year+'_'+u_bbs_month
                u_db[archive_bbs_collection].insert(u_bbs)
                u_db['user_bbs'].remove(u_bbs)
                count = count+1
            print 'count: ',count
            print 'remove...'
            #u_db['user_bbs'].remove({user_bbs_time:{'$lt':currentTime}})
        else:
            print 'no bbs!'
        ###
        u_collection_blog = u_db['user_blog']
        print 'user blog'
        if u_collection_blog.count() > 0:
            ###find data in last month
            for u_blog in u_db['user_blog'].find({user_blog_time:{'$lt':currentTime}}).limit(limits):
                #print u_blog
                u_blog_time = u_blog[user_blog_time]
                #print u_blog_time
                u_blog_year = u_blog_time[0:4]
                u_blog_month = u_blog_time[5:7]
                #print u_blog_year,u_blog_month
                archive_blog_collection = 'user_blog_'+u_blog_year+'_'+u_blog_month
                u_db[archive_blog_collection].insert(u_blog)
                u_db['user_blog'].remove(u_blog)
                count = count+1
            print 'count: ',count
            print 'remove...'
            #u_db['user_blog'].remove({user_blog_time:{'$lt':currentTime}})
        else:
            print 'no blog!'
        ###
        u_collection_np = u_db['user_np']
        print 'user np'
        if u_collection_np.count() > 0:
            ###find data in last month
            for u_np in u_db['user_np'].find({user_np_time:{'$lt':currentTime}}).limit(limits):
                #print u_np
                u_np_time = u_np[user_np_time]
                #print u_np_time
                u_np_year = u_np_time[0:4]
                u_np_month = u_np_time[5:7]
                #print u_np_year,u_np_month
                archive_np_collection = 'user_np_'+u_np_year+'_'+u_np_month
                u_db[archive_np_collection].insert(u_np)
                u_db['user_np'].remove(u_np)
                count = count+1
            print 'count: ',count
            print 'remove...'
            #u_db['user_np'].remove({user_np_time:{'$lt':currentTime}})
        else:
            print 'no np!'
        ###
        u_collection_nw = u_db['user_nw']
        print 'user nw'
        if u_collection_nw.count() > 0:
            ###find data in last month
            for u_nw in u_db['user_nw'].find({user_nw_time:{'$lt':currentTime}}).limit(limits):
                #print u_nw
                u_nw_time = u_nw[user_nw_time]
                #print u_nw_time
                u_nw_year = u_nw_time[0:4]
                u_nw_month = u_nw_time[5:7]
                #print u_nw_year,u_nw_month
                archive_nw_collection = 'user_nw_'+u_nw_year+'_'+u_nw_month
                u_db[archive_nw_collection].insert(u_nw)
                u_db['user_nw'].remove(u_nw)
                count = count+1
            print 'count: ',count
            print 'remove...'
            #u_db['user_nw'].remove({user_nw_time:{'$lt':currentTime}})
        else:
            print 'no nw!'
        ###
    conn.close()
        
#########################################
def mongodbArchiveTool():
    global count
    while(1):
        ###
        count = 0
        start_time =  time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
        hour_time = time.strftime('%H',time.localtime(time.time()))
        if hour_time >= '00' and hour_time <= '07':
            print 'hour_time : ',hour_time
        
            #
            readAllUsersFromMongoDB()
            archiveMongodbEveryMonth()
            #
            end_time =  time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
            fp = open(log_file, 'a')
            fp.write('###')
            fp.write(start_time+'###')
            fp.write(end_time+'###')
            fp.write(str(count))
            fp.write('###')
            fp.write('\n')
            fp.close()
        else:
            print 'hour_time : ',hour_time
        ###
        sleeptime =  random.randint(1800, 3600)
	print('sleeping...'+str(sleeptime))
        time.sleep(sleeptime)
    
#########################################  
if __name__ == '__main__':
    mongodbArchiveTool()
    
